README.html: README.md readable.css
	pandoc $< --standalone --from=markdown+smart --to=html --css readable.css --output=$@

docs: optimism.py
	PYTHONPATH=. pdoc --output-dir docs optimism
