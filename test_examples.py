import os
import sys
import io
import importlib
import re

import optimism

optimism.skipChecksAfterFail(None)
optimism.suppressErrorDetailsAfterFail(None)

# Detect whether we've got better or worse line numbers so we can create
# correct tests...
better_line_numbers = sys.version_info >= (3, 8)

EXAMPLES = [
    ("basic", []),
    ("bits", []),
    ("multi", []),
    ("block", []),
    ("runfile", ["Tester"]),
    ("tracing", ["Tester"]),
    ("catch_and_release", ["one", "two"]),
    ("skip", []),
    ("fragments", []),
    ("errors", []),
    ("files", []),
    ("code", []),
    ("suites", []),
    #("reverse_tracing", []),
    # TODO: Include this once the machinery for it is there
]


EXPECTATIONS = {
    "basic": [
        "✓ {file}:16",
        "✓ {file}:27",
        "✓ {file}:42",
        "✓ {file}:43" if better_line_numbers else "✓ {file}:45",
        "---\nAll 4 expectation(s) were met.\n---",
        """\
✗ {file}:54
  Result:
    11
  was NOT equivalent to the expected value:
    10
  Called function 'f' with arguments:
    x = 6
    y = 4""",
        """\
✗ {file}:55
  Result:
    11
  was NOT equivalent to the expected value:
    10
  Test expression was:
    f(x + x, y)
  Values were:
    x = 3
    y = 4""",
        """\
✗ expectation from {file}:61 NOT met for test case at {file}:60
  Result:
    None
  was NOT equivalent to the expected value:
    False
  Called function 'display' with arguments:
    message = 'nope'""",
        """\
✓ expectation from {file}:62 met for test case at {file}:60
  Printed lines:
    \"\"\"\\
    The message is:
    -nope-
    \"\"\"
  were exactly the same as the expected printed lines:
    \"\"\"\\
    The message is:
    -nope-
    \"\"\"
  Called function 'display' with arguments:
    message = 'nope'""",
        """\
✗ {file}:65
  Printed lines:
    \"\"\"\\
    The message is:
    -nope-
    \"\"\"
  were NOT the same as the expected printed lines:
    \"\"\"\\
    The message is:
    -yep-
    \"\"\"
  First difference was:
    strings differ on line 2 where we got:
      '-nope-'
    but we expected:
      '-yep-'
  Called function 'display' with arguments:
    message = 'nope'""",
        """\
✗ {file}:69
  Result:
    1
  was NOT equivalent to the expected value:
    5
  Called function 'f' with arguments:
    x = 0
    y = 0""",
        """\
✗ {file}:69
  Result:
    3
  was NOT equivalent to the expected value:
    5
  Called function 'f' with arguments:
    x = 1
    y = 1""",
        "✓ {file}:69",
        """\
---
6 of the 12 expectation(s) were NOT met:
  ✗ {file}:54
  ✗ {file}:55
  ✗ {file}:61
  ✗ {file}:65
  ✗ {file}:69
  ✗ {file}:69
---"""
    ],

    "bits": [
        """\
✓ {file}:9
  Result:
    [1, 2, 3, [4, 5, 6]]
  was equivalent to the expected value:
    [1, 2, 3, [4, 5, 6]]
  Test expression was:
    x
  Values were:
    x = [1, 2, 3, [4, 5, 6]]""",
        """\
✓ {file}:12
  Result:
    [1, 2, 3, [4, 5]]
  was equivalent to the expected value:
    [1, 2, 3, [4, 5]]
  Test expression was:
    x
  Values were:
    x = [1, 2, 3, [4, 5]]""",
        """\
✓ {file}:13
  Result:
    [4, 5]
  was equivalent to the expected value:
    [4, 5]
  Test expression was:
    x[3]
  Values were:
    x = [1, 2, 3, [4, 5]]
    x[3] = [4, 5]""",
        """\
✓ {file}:14
  Result:
    5
  was equivalent to the expected value:
    5
  Test expression was:
    x[3][-1]
  Values were:
    x = [1, 2, 3, [4, 5]]
    x[3] = [4, 5]
    x[3][-1] = 5""",
        """\
✓ {file}:17
  Result:
    [1, 2, 3]
  was equivalent to the expected value:
    [1, 2, 3]
  Test expression was:
    x
  Values were:
    x = [1, 2, 3]""",
        """\
✓ {file}:18
  Result:
    2
  was equivalent to the expected value:
    2
  Test expression was:
    x[1]
  Values were:
    x = [1, 2, 3]
    x[1] = 2""",
        """\
✓ {file}:20
  Result:
    [1, 2, 3]
  was equivalent to the expected value:
    [1, 2, 3]
  Test expression was:
    x
  Values were:
    x = [1, 2, 3]""",
        """\
✓ {file}:22
  Result:
    [1, 2]
  was equivalent to the expected value:
    [1, 2]
  Test expression was:
    x
  Values were:
    x = [1, 2]""",
        """\
✓ {file}:25
  Result:
    2
  was equivalent to the expected value:
    2
  Test expression was:
    x[-1]
  Values were:
    x = [1]
    x[-1] = 1""",
        """\
✓ {file}:27
  The result type (<class 'list'>) was the expected type.
  Test expression was:
    x
  Values were:
    x = [1]""",
        """\
✓ {file}:28
  The result type (<class 'int'>) was the expected type.
  Test expression was:
    x[0]
  Values were:
    x = [1]
    x[0] = 1""",
    ],

    "block": [
        """\
✗ {file}:12
  Failed due to an error:
    Traceback (most recent call last):
      File "{opt_file}", line X, in _run
        value, scope = payload()
      File "{opt_file}", line X, in payload
        exec(self.manager.target, env)
      File "<string>", line X, in <module>
    NameError: name 'x' is not defined
  Ran code:
    print(x)
    x += 1
    print(x)""",
        """\
✗ {file}:13
  Failed due to an error:
    Traceback (most recent call last):
      File "{opt_file}", line X, in _run
        value, scope = payload()
      File "{opt_file}", line X, in payload
        exec(self.manager.target, env)
      File "<string>", line X, in <module>
    NameError: name 'x' is not defined
  Ran code:
    print(x)
    x += 1
    print(x)""",
        "✓ {file}:15",
        "✓ {file}:16",
        "✓ {file}:28",
        "✓ {file}:29",
        """\
✗ {file}:30
  Variable 'x' with value:
    5
  was NOT equivalent to the expected value:
    6
  Ran code:
    x = 3
    print(x)
    x += 1
    print(x)
    x += 1""",
    ],

    "multi": [
        f"""\
✓ {{file}}:7
  Result:
    {eval(expr)}
  was equivalent to the expected value:
    {eval(expr)}
  Test expression was:
    {expr}
  Values were:
    i = {i}"""
        for i in range(3)
        for expr in ["i", "i + 1", "i * i"]
    ] + [
         """\
✓ {file}:9
  Result:
    (1, 2, 3)
  was equivalent to the expected value:
    (1, 2, 3)
  Test expression was:
    (
            1,
            2,
            3
        )""" if better_line_numbers else """\
✓ {file}:15
  Result:
    (1, 2, 3)
  was equivalent to the expected value:
    (1, 2, 3)
  Test expression was:
    (
        1,
        2,
        3
    )""",
    ],

    "runfile": [
        "✓ {file}:9" if better_line_numbers else "✓ {file}:12",
        "✓ {file}:35",
        """\
✗ {file}:42
  Custom check failed:
    failure message
  Ran file 'io_example.py'""",
        "✓ {file}:57",
        """\
✗ {file}:58
  Custom check failed:
    'Hi Tester!' did not match 'Hi Tester?'
  Ran file 'io_example.py'""",
    ],

    "tracing": [
        "3 input(\"What is your name? \") ⇒ 'Tester'",
        "4 name ⇒ 'Tester'",
        "4 \"Hello \" + opt.trace(name) ⇒ 'Hello Tester'",
        "5 '=' * len(greeting) ⇒ '============'",
        "6 print(greeting) ⇒ None",
        "7 underline ⇒ '============'",
        "9 \"ha\" * 50 ⇒ 'hahahahahahahahahahahahahahahahahaha...",
        """\
{file}:12 "ha" * 50 ⇒ 'hahahahahahahahahahahahahahahahahaha...
  Full result is:
    'hahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahaha'""", # noqa
    ],

    "catch_and_release": [
        "✓ {file}:19",
        "✓ {file}:20",
        "✓ {file}:24",
        "✓ {file}:25",
        "✓ {file}:30",
        "✓ {file}:31",
    ],

    "skip": [
        "✓ {file}:4", # appears twice
        "Did not find 'epxetc' in module 'optimism'...",
        "~ {file}:7 (skipped)",
        "~ {file}:8 (skipped)",
        """\
✗ skip.py:19
  Result:
    3
  was NOT equivalent to the expected value:
    4
  Called function 'f'""",
        "~ skip.py:20 (skipped)",
        "~ skip.py:21 (skipped)",
        "✓ skip.py:24",
        """\
✗ skip.py:29
  Result:
    3
  was NOT equivalent to the expected value:
    4
  Called function 'f'""",
        "~ skip.py:30 (skipped)",
        "~ skip.py:31 (skipped)",
        """\
✗ skip.py:36
  Result:
    3
  was NOT equivalent to the expected value:
    4
  Called function 'f'""",
        """\
✗ skip.py:37
  Result:
    3
  was NOT equivalent to the expected value:
    5
  Called function 'f'""",
        "✓ skip.py:38",
    ],

    "fragments": [
        "✓ {file}:19",
        "✓ {file}:20",
        "✓ {file}:21",
        "✓ {file}:22",
        "✓ {file}:23",
    ],

    "errors": [
        """\
✗ {file}:6
  Failed due to an error:
    Traceback (most recent call last):
      File "{opt_file}", line X, in _run
        value, scope = payload()
      File "{opt_file}", line X, in payload
        self.manager.target(*self.args, **self.kwargs),
      File "{dir}./bad.py", line X, in f
        z = 3 / 0
    ZeroDivisionError: division by zero
  Called function 'f'"""
    ],

    "files": [
        "✓ {file}:10",
        "✓ {file}:11",
        '''\
✗ {file}:12
  File contents:
    """\\
    1
    2
    3
    """
  were NOT the same as the expected file contents:
    """\\
    1
    2
    """
  First difference was:
    strings differ on line 3 where we got:
      '3'
    but we expected:
      ''
  Called function 'write123' with arguments:
    filename = 'b.txt'
''',
        '''\
✗ {file}:13
  File contents:
    """\\
    1
    2
    3
    """
  were NOT the same as the expected file contents:
    """\\
    1
    2
    hello
    """
  First difference was:
    strings differ on line 3 where we got:
      '3'
    but we expected:
      'hello'
  Called function 'write123' with arguments:
    filename = 'b.txt'
''',
        "✓ {file}:23",
        '''\
✗ {file}:25
  File contents:
    """\\
    abc\\r\\r
    def\\r\\r
    """
  were NOT the same as the expected file contents:
    """\\
    abc
    def
    """
  First difference was:
    strings differ on line 1 where we got:
      'abc\\r\\r'
    but we expected:
      'abc'
  Called function 'writeReturny' with arguments:
    filename = 'a.txt'
''',
        "✓ {file}:37",
        "✓ {file}:39",
        "✓ {file}:46",
        '''\
✗ {file}:48
  Result:
    'abc\\ndef'
  was NOT equivalent to the expected value:
    'abc\\ndef\\n'
  Test expression was:
    'abc\\ndef'
''',
    ],

    "code": [
        "✓ {file}:20",
        "✓ {file}:23",
        "✓ {file}:24",
        "✓ {file}:25",
        "✓ {file}:34",
        "✓ {file}:35",
        """\
✗ {file}:36
  Code does not contain the expected structure:
    at least 1 definition(s) of functionName
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: FunctionDef on line 9
  checked code in file '{full_file}'""",
        "✓ {file}:38",
        """\
✗ {file}:39
  Code does not contain the expected structure:
    at least 1 function definition(s) containing:
      at least 1 loop(s) or generator expression(s)
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: FunctionDef on line 9
  checked code in file '{full_file}'""",
        """\
✗ {file}:40
  Code does not contain the expected structure:
    at least 1 loop(s) or generator expression(s) containing:
      at least 1 function definition(s)
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: For on line 28
  checked code in file '{full_file}'""",
        """\
✗ {file}:41
  Code does not contain the expected structure:
    at least 1 while loop(s)
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: For on line 28
  checked code in file '{full_file}'""",
        "✓ {file}:42",
        "✓ {file}:44" if better_line_numbers else "✓ {file}:45",
        "✓ {file}:47" if better_line_numbers else "✓ {file}:48",
        "✓ {file}:50" if better_line_numbers else "✓ {file}:51",
        """\
✗ {file}:""" + '53' if better_line_numbers else '54' + """
  Code does not contain the expected structure:
    at least 1 loop(s) or generator expression(s) containing:
      4 call(s) to print
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: For on line 28
  checked code in file '{full_file}'""",
        """\
✗ {file}:""" + '56' if better_line_numbers else '57' + """
  Code does not contain the expected structure:
    at least 1 loop(s) or generator expression(s) containing:
      at least 4 call(s) to print
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: For on line 28
  checked code in file '{full_file}'""",
        "✓ {file}:59" if better_line_numbers else "✓ {file}:60",
        """\
✗ {file}:""" + '62' if better_line_numbers else '63' + """
  Code does not contain the expected structure:
    at least 1 loop(s) or generator expression(s) containing:
      no call(s) to print
  Although it does partially satisfy the requirement:
    Requirement partially satisfied via 0 full and 1 partial match(es):
      Partial match: For on line 28
  checked code in file '{full_file}'""",
        "✓ {file}:65" if better_line_numbers else "✓ {file}:66",
    ],

    "reverse_tracing": [
        # TODO
    ],

    "suites": [
        "✓ {file}:14",
        "✓ {file}:16",
        "✓ {file}:17",
        "✓ {file}:20",
        "✓ {file}:24",
        '''\
✗ {file}:26
  Printed lines:
    """\\
    4
    """
  were NOT the same as the expected printed lines:
    """\\
    5
    """
  First difference was:
    strings differ on line 1 where we got:
      '4'
    but we expected:
      '5'
  Called function 'f' with arguments:
    x = 4
    y = 5''',
        "✓ {file}:27",
        "✓ {file}:29",
        """\
---
1 of the 4 expectation(s) were NOT met:
  ✗ {file}:26
---""",
        """\
---
All 4 expectation(s) were met.
---""",
    ]
}

STDOUT_EXPECT = {
    "catch_and_release": [
        "one Z!",
        "Re-providing input was blocked.",
        "Hello",
        "two Z!"
    ],

    "code": [
        "0",
        "1",
        "2",
        "3"
    ],

    "suites": [
        '3',
        'a',
        """\
Outcomes in suite A:
(True, '{file}:14', '✓ {file}:14')
(True, '{file}:16', '✓ {file}:16')
(True, '{file}:17', '✓ {file}:17')
(True, '{file}:20', '✓ {file}:20')""",
        '''\
Outcomes in suite B:
(True, '{file}:24', '✓ {file}:24')
(False, '{file}:26', '✗ {file}:26\\n  Printed lines:\\n    """\\\\\\n    4\\n    """\\n  were NOT the same as the expected printed lines:\\n    """\\\\\\n    5\\n    """\\n  First difference was:\\n    strings differ on line 1 where we got:\\n      \\\'4\\\'\\n    but we expected:\\n      \\\'5\\\'\\n  Called function \\\'f\\\' with arguments:\\n    x = 4\\n    y = 5')
(True, '{file}:27', '✓ {file}:27')
(True, '{file}:29', '✓ {file}:29')''',
        """\
Outcomes in suite A (again):
(True, '{file}:14', '✓ {file}:14')
(True, '{file}:16', '✓ {file}:16')
(True, '{file}:17', '✓ {file}:17')
(True, '{file}:20', '✓ {file}:20')
(True, '{file}:46', '✓ {file}:46')""",
        "No more outcomes in suite A: []",
        "No more outcomes in suite B: []",
        "No more suite B.",
    ]
}


def run_example(mname, inputs=[]):
    """
    Runs a module and captures stderr and stdout, returning a tuple
    containing the imported module, the captured stderr string, and the
    captured stdout string. The given input strings are provided as stdin
    during the run.
    """
    old_stderr = sys.stderr
    old_stdout = sys.stdout
    old_stdin = sys.stdin
    old_print_to = optimism.PRINT_TO
    sys.stderr = io.StringIO()
    sys.stdout = io.StringIO()
    sys.stdin = io.StringIO('\n'.join(inputs) + '\n')
    optimism.PRINT_TO = sys.stderr

    filePath = os.path.abspath(mname + '.py')

    try:
        spec = importlib.util.spec_from_file_location(mname, filePath)
        module = importlib.util.module_from_spec(spec)
        # Note we skip adding it to sys.modules
        spec.loader.exec_module(module)
        errOut = sys.stderr.getvalue()
        stdOut = sys.stdout.getvalue()

    finally:
        sys.stderr = old_stderr
        sys.stdout = old_stdout
        sys.stdin = old_stdin
        optimism.PRINT_TO = old_print_to

    return (module, errOut, stdOut)


def test_all():
    """Tests each example file."""
    os.chdir("examples")
    sys.path.insert(0, '.')
    for (mname, inputs) in EXAMPLES:
        optimism.detailLevel(0)
        optimism.colors(False)
        module, output, stdout = run_example(mname, inputs)
        # Scrub line numbers from tracebacks so our expectations don't
        # become hopelessly fragile
        stdout = re.sub(r", line \d+,", ", line X,", stdout)
        output = re.sub(r", line \d+,", ", line X,", output)
        for ex in EXPECTATIONS[mname]:
            exp = ex.format(
                file=os.path.basename(module.__file__),
                dir=(
                    os.path.split(module.__file__)[0] + '/'
                    if better_line_numbers
                    else ''
                ),
                full_file=module.__file__,
                opt_file=optimism.__file__,
            )
            # Redundant, but makes issues easier to diagnose
            for line in exp.splitlines():
                assert(line in output), (line, output)
            assert (exp in output), output
        if mname in STDOUT_EXPECT:
            for ex in STDOUT_EXPECT[mname]:
                exp = ex.format(
                    file=os.path.basename(module.__file__),
                    dir=os.path.split(module.__file__)[0],
                    full_file=module.__file__,
                    opt_file=optimism.__file__,
                )
                for line in exp.splitlines():
                    assert(line in stdout), stdout
                assert (exp in stdout), stdout
    os.chdir("..")
